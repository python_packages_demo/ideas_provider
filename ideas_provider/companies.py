from lxml import html
import urllib.request as ur
import random


def suggest_company() -> str:
    response = ur.urlopen("https://en.wikipedia.org/wiki/List_of_S%26P_500_companies")
    html_text = response.read().decode('utf-8')
    tree = html.fromstring(html_text)
    codes = tree.xpath('//tr/td[1]/a/text()')
    return random.choice(codes)
    # names = tree.xpath('//tr/td[2]/a/text()')
    # index = random.randint(0, len(codes)-1)
    # return codes[index], names[index]


if __name__ == '__main__':
    code = suggest_company()
    print(code)
