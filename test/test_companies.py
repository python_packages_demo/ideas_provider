import unittest
import random

from ideas_provider import suggest_company


class Test(unittest.TestCase):
    def test_companies(self):
        random.seed(1)
        company = suggest_company()
        self.assertEqual('BX', company)
        #self.assertEqual(('BX', 'Blackstone'), company)
